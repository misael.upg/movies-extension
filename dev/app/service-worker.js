const STATIC_CACHE_NAME = 'moviesApp-v1';

self.addEventListener('install', function(event) {
  // TODO: cache /skeleton rather than the root page
  event.waitUntil(
    caches.open(STATIC_CACHE_NAME).then(function(cache) {
      console.log('Opened cache');
      return cache.addAll([
        'index.html',
        'images/google-logo.svg',
        'scripts/main.min.js',
        'styles/material.min.css',
        'styles/main.css',
        'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700',
        'https://fonts.googleapis.com/icon?family=Material+Icons',
        'scripts/firebase.js',
      ]);
    })
  );
});

self.addEventListener('activate', function(event) {
  event.waitUntil(
    caches.keys().then(function(cacheNames) {
      return Promise.all(
        cacheNames.filter(function(cacheName) {
          return cacheName.startsWith('moviesApp-') &&
                 cacheName != STATIC_CACHE_NAME;
        }).map(function(cacheName) {
          return caches.delete(cacheName);
        })
      );
    })
  );
});

self.addEventListener('fetch', function(event) {
  // TODO: respond to requests for the root page with
  // the page skeleton from the cache
  let requestURL = new URL(event.request.url);
  if (requestURL.origin === location.origin) {
    if (requestURL.pathname === '/') {
      event.respondWith(caches.match('/index.html'));
      return;
    }
  }

  event.respondWith(
    caches.match(event.request).then(function(response) {
      return response || fetch(event.request);
    })
  );
});

self.addEventListener('message', function(event) {
  if (event.data.action === 'skipWaiting') {
    self.skipWaiting();
  }
});
