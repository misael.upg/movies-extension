/** Class to manipulate the DOM */
class View {
    
    /**
     * @param  {string} containerId - A string as referencing the id of the containerId element
     */
    constructor(containerId) {
        this.containerId = document.getElementById(`${containerId}`);
    }
    static showSnackBar(message, actionHandler, actionText) {
        const notification = document.querySelector('.mdl-js-snackbar');
        let data = {
        message: message,
        actionHandler: actionHandler,
        actionText: actionText,
        timeout: 5000,
        };
        notification.MaterialSnackbar.showSnackbar(data);
    }
    /** Appends a movie list item to a list
     * @param  {string} movieName - The movie title
     * @param  {string} imgSrc - The movie smallest thumbnail url
     */
    static appendItem(parentNode, template) {
        parentNode.appendChild(template);
    }

    static removeItem(item) {
        item.remove();
    }
    
    static preppendItem(parentNode, template) {
        parentNode.insertBefore(template, parentNode.firstChild);
    }
    /** Append a list of items to a HTML node
     * @param  {HTMLNode} parentNode
     * @param  {iterable} data - Data to be rendered in a template
     * @param  {object} template - An function object from the TEMPLATES class
     */
    async displayElements(parentNode, data, template) {
        // Ierate through data and append to node
        for (let object of data) {
            let key = await Controller.checkIfSaved(object.id);
            if(key) {
                object.key = key;
                object.isSaved = true;
            }
            View.appendItem(parentNode, template(object));
        }
    }
    // Needed for saved elements container
    async displayElements2(parentNode, data, template) {
        // Ierate through data and append to node
        for (let object of data) {
            // Needed for icon
            object.isSaved = true;
            View.appendItem(parentNode, template(object));
        }
    }
    // Remove and insert movie details to DOM
    static toggleMovieDetails(e) {
        if (e.target.parentNode.nodeName != 'A') {
            const movieId = this.getAttribute('movie_id');
            // Get saved details data from model
            let movieDetails = Model.getMovie(movieId);
            // Remove active class for previous element
            let previousActiveNodes = this.parentNode.getElementsByClassName('active');
            // If the user hasn't click on any item (first time click), and the last clicked element is not 
            // the same we are clikcing is not the same as the store in the previousActiveNodes array.
            if(previousActiveNodes.length > 0 && previousActiveNodes[0] !== this ) {
                let movieDetails1 = previousActiveNodes[0].getElementsByClassName('movie-details-1')[0];
                let movieDetails2 = previousActiveNodes[0].getElementsByClassName('movie-details-2')[0];
                let movieDetails3 = previousActiveNodes[0].getElementsByClassName('movie-details-3')[0];
                previousActiveNodes[0].classList.toggle('active');
                TEMPLATES.movieListItem.removeDetails([movieDetails1, movieDetails2, movieDetails3]);
            } // else {
                // Trigger styles and animations, and to allow inserting new nodes
                this.classList.toggle('active');
                // Get nodes where we are going to append new nodes
                let movieDetails1 = this.getElementsByClassName('movie-details-1')[0];
                let movieDetails2 = this.getElementsByClassName('movie-details-2')[0];
                let movieDetails3 = this.getElementsByClassName('movie-details-3')[0];
                // check if we already append details to remove them before inserting again
                if(movieDetails1) {
                    TEMPLATES.movieListItem.removeDetails([movieDetails1, movieDetails2, movieDetails3]);
                }
                // Status must be active
                if(this.classList.contains('active')) {
                    let movieDetailsNode = this.getElementsByClassName('movie-details')[0];
                    // Update cover image with a bigger resolution
                    let imageEl = this.getElementsByClassName('mdl-list__item-avatar')[0];
                    imageEl.setAttribute('src', this.getAttribute('medium_cover_image'));
                    // Get data from HTML elment attributes
                    let details1Object = {
                        year: this.getAttribute('year'),
                        rating: this.getAttribute('rating'),
                        yt_trailer_code: this.getAttribute('yt_trailer_code')
                    };
                    // This can't be in the previous object because needs to be in another template
                    let summary = this.getAttribute('summary');
                    View.appendItem(movieDetailsNode, TEMPLATES.movieDetails1(details1Object));
                    View.appendItem(this, TEMPLATES.movieDetails2({summary: summary}));
                    View.appendItem(this, TEMPLATES.movieDetails3());
                    for (let castMember of movieDetails.cast) {
                        View.appendItem(this.getElementsByClassName('movie-details-3')[0], TEMPLATES.movieCastMember(castMember));
                    }
                }
            // }
        }
    }
}

// Define a set of templates to be used inside the view, every template
// is a function object that returns an HTMLElements containing data properties embeded
const TEMPLATES = {
    movieListItem: (data) => {
        console.log(data.title, data.key);
        const el = document.createElement('div');
        let iconName = 'add_circle_outline';
        if(data.isSaved) {
            iconName = 'remove_circle';
        }
        el.classList.add('mdl-list__item');
        el.innerHTML = `<div class="item-content-container">
                        <span class="mdl-list__item-primary-content">
                        <img src="${data.small_cover_image}" class="mdl-list__item-avatar">
                        <span class="movie-details"><span class="movie-title-small">
                        ${data.title}</span>
                        </span>
                        </span>
                        <a class="mdl-list__item-secondary-action save-later">
                        <i class="material-icons add-remove-icon">${iconName}</i>
                        </a>
                        <div>
                        `;
        // Add event listeners here
        el.onclick = View.toggleMovieDetails;
        // ad event listener to bookmark icon to save movie
        el.querySelector('.save-later').addEventListener('click', () => { 
            Controller.saveForLater(data, event.target);
        });
        // Bind data properties to element
        el.setAttribute('medium_cover_image', `${data.medium_cover_image}`);
        el.setAttribute('year', `${data.year}`);
        el.setAttribute('rating', `${data.rating}`);
        el.setAttribute('summary', `${data.summary}`);
        el.setAttribute('movie_id', `${data.id}`);
        el.setAttribute('yt_trailer_code', `${data.yt_trailer_code}`);
        return el;
    },
    popularMovieItem: (data) => {
        const el = document.createElement('div');
        el.classList.add('popular-movie-item');
        el.innerHTML = `<div class="popular-movie-item">
                        <img src="${data.medium_cover_image}" 
                        class="popular-movie__poster">
                        </img>
                        <h3>${data.title}</h3>
                        </div>`;
        return el;
    },
    movieDetails1: (data) => {
        const el = document.createElement('div');
        el.classList.add('movie-details-1');
        el.innerHTML = `<span class="movie-year">${data.year}</span>
                        <span class="movie-rating">${data.rating}</span>
                        <a href="https://www.youtube.com/watch?v=${data.yt_trailer_code}" 
                        target="_blank"
                        class="mdl-button play-trailer-button mdl-js-button mdl-js-ripple-effect mdl-button-accent">
                        <i class="material-icons">play_arrow</i>
                        <span>Watch trailer</span>
                        </a>`;
        return el;
    },
    movieDetails2: (data) => {
        const el = document.createElement('div');
        el.classList.add('movie-details-2');
        el.innerHTML = `<div class="movie-summary"><p>${data.summary}</p></div>`;
        return el;
    },
    movieDetails3: (data={}) => {
        const el = document.createElement('div');
        el.classList.add('movie-details-3');
        return el;
    },
    movieCastMember: (data) => {
        const el = document.createElement('a');
        el.classList.add('cast-member', 'mdl-chip', 'mdl-chip--contact');
        el.setAttribute('href', `http://www.google.com/search?q=${data.name}`);
        if(data.url_small_image) {
            el.innerHTML = `<span class="mdl-chip__contact" 
                            style="background-image:url(${data.url_small_image});background-size:cover;"></span>
                            <span class="mdl-chip__text">${data.name}</span>`;
        } else {
            el.innerHTML = `<span class="mdl-chip__contact mdl-color--teal mdl-color-text--white">A</span>
                            <span class="mdl-chip__text">${data.name}</span>`;
        }
        return el;
    },
    profileInfo: (user) => {
        const el = document.createElement('div');
        el.setAttribute('id', 'profileInfo');
        el.classList.add('profile-info');
        el.innerHTML =
        `<div class="user-info">
            <img class="profile-picture" src="${user.photoURL}" alt="profile picture">
            <span class="profile-name">${user.displayName}</span>
        </div>
        <div>
            <button id="logoutButton" class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button-accent">
                Logout
            </button>
        </div>`;
        return el;
    },
};

// add custom methods for each template
TEMPLATES.movieListItem.removeDetails = (items) => {
    for(let item of items) {
        item.remove();
    }
};