/** Interface to acces YTS API */
class Model {
    constructor(data) {
        this.data = data;
    }
    static getMovie(id) {
        return this.data.find((item) => item.id == id);
    }
    /** Get the latest movies from YTS
     * @param  {number} limit - The number to limit the number of movies fetched, with 40 as the default
     * @return {promise} A promise to resolve with the latest movies from YTS
     */
    static getLatestMovies(limit=40) {
        return fetch(`https://yts.ag/api/v2/list_movies.json?limit=${limit}`)
            .then((response) => {
                if (response.ok) {
                    return response.json();
                } else {
                    console.log(response.status);
                }
            });
    }
    /** Get movie details
     * @param  {number} id - Movie ID from YTS
     * @return {promise} A promise to fetch movie detals
     */
    static fetchMovieDetails(id) {
        return fetch(`https://yts.ag/api/v2/movie_details.json?movie_id=${id}&with_cast=true`)
            .then((response) => {
                if (response.ok) {
                    return response.json();
                } else {
                    console.log(response.status);
                }
            });
    }

    static saveData(data) {
        this.data = data;
    }

}

class Local {}