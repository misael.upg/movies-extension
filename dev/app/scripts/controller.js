/** Interface between View and Model
 *  A controller
 */
class Controller {
  /**
   * @param  {string} containerId - Container element for View
   */
  constructor(containerId) {
    // Change this later. It is referenceing only one container, but
    // controller doesn't need this in constructor. It's not that unique
    // or important because it a partial from the DOM, and not unique toone
    // view.
    this.containerId = containerId;
    // think twice about this, almost all methods on View are static,
    // there's actually only one View, and not two as I thought, there´re
    // two main containers, but only one view.
    this.view;
    // To save movies once thay have been downloaded
    this.data = {};
    this.init();
    // Refernce to Firebase SDK methods
    this.auth;
    this.database;
    this.storage;
    this.currentUser;
  }
  // Initalize Firebase SDK, create a View, and get movies
  init() {
    this.initFirebase();
    this.createView();
    this.getMovies();
  }
  initFirebase() {
    // Setup Firebase SDK
    let config = {
      apiKey: 'AIzaSyDB_IUKBbGA2-Ui_oftG3d-gKTUp_kYaLg',
      authDomain: 'movies-6af85.firebaseapp.com',
      databaseURL: 'https://movies-6af85.firebaseio.com',
      storageBucket: 'movies-6af85.appspot.com',
      messagingSenderId: '694452606519',
    };
    firebase.initializeApp(config);
    // Shortcuts to Firebase SDK features.
    this.auth = firebase.auth();
    this.database = firebase.database();
    this.storage = firebase.storage();
    // Bind Firebase method to class method
    this.auth.onAuthStateChanged(this.onAuthStateChanged.bind(this));
  }
  // Sign user in with redirect method
  signin() {
    // We usee Google as our provider
    let provider = new firebase.auth.GoogleAuthProvider();
    this.auth.signInWithRedirect(provider);
  }
  signOut() {
    this.auth.signOut();
    this.cleanSaved();
    this.cleanIcons();
  }
  getSavedMovies() {
    // If user is signed in, get movies from DB
    if (Controller.checkSignedIn()) {
      firebase.database().ref('users/' +
          firebase.auth().currentUser.uid +
          '/movies/').once('value')
        .then((snapshot) => {
          // Set a new object for custom properties
          let movies = [];
          // For each item from DB, add to movies cstom object with
          // key from DB item
          snapshot.forEach((childSnapshot) => {
            let movieObject = childSnapshot.val();
            movieObject.key = childSnapshot.getKey();
            movies.push(movieObject);
          });
          // display from most recent to latest added
          this.displaySavedMovies(movies.reverse());
        })
        .catch((error) => {
          console.log(error);
        });
    } else {
      // Insert here the code to inform the user to sign in.
      console.log('User not logged in');
    }
  }
  cleanIcons() {
    const icons = document.getElementsByClassName('add-remove-icon');
    [].forEach.call(icons, (icon) => {
      icon.innerHTML = 'add_circle_outline';
    });
  }
  // Can´t be asyncronous because of the while loop
  cleanSaved() {
    const container = document.getElementById('list-container-saved');
    while (container.hasChildNodes()) {
      container.removeChild(container.lastChild);
    }
  }
  // insert into DOM saved movies
  displaySavedMovies(movies) {
    let containerEl = document.getElementById('list-container-saved');
    this.view.displayElements2(
      containerEl,
      movies,
      TEMPLATES.movieListItem);
  }
  // Only after initialization, not useful for redirects and first
  // sign in
  static checkSignedIn() {
    if (firebase.auth().currentUser) {
      console.log('User signed in');
      return true;
    } else {
      console.log('User not logged in');
      return false;
    }
  }
  static async checkIfSaved(movieId) {
    // If user not login wait for redirect result, so we can add the right
    // icon
    if (!firebase.auth().currentUser) {
      await firebase.auth().getRedirectResult();
    }
    if (firebase.auth().currentUser) {
      return new Promise((resolve, reject) => {
        firebase.database().ref('users/' +
            firebase.auth().currentUser.uid +
            '/movies/'
          )
          .orderByChild('id')
          .equalTo(movieId).once('value', function (snapshot) {
            let movieData = snapshot.val();
            if (movieData) {
              // Get the Firebase child key from snapshot object
              // as the name of the first object property, since
              // the key firebase returns is the one from the parent,
              // not the child. It is kind of hacky.
              let key = Object.getOwnPropertyNames(movieData)[0];
              console.log('Movie found in DB');
              resolve(key);
            } else {
              console.log('Movie not found in DB');
              resolve(false);
            }
          });
      });
    } else {
      console.log('here', firebase.auth().user);
      return false;
    }
  }
  // Data is helpful to get the right DOM element in saved tab
  static removeFromDB(key, eventTarget, movieData) {
    if (firebase.auth().currentUser) {
      let reference = firebase.database().ref('users/' +
        firebase.auth().currentUser.uid +
        '/movies/' +
        key);
      reference.remove()
        .then(() => {
          let containerSaved = document.getElementById('list-container-saved');
          let itemToRemove = containerSaved.querySelector(`div[movie_id="${movieData.id}"]`);
          eventTarget.innerHTML = 'add_circle_outline';
          View.removeItem(itemToRemove);
          // Also need to update icon indide Now container
          let containerNow = document.getElementById('list-container');
          let itemToUpdate = containerNow.querySelector(`div[movie_id="${movieData.id}"]`);
          let icon = itemToUpdate.getElementsByClassName('add-remove-icon')[0];
          icon.innerHTML = 'add_circle_outline';
          console.log('Item remove from DB');
        })
        .catch((e) => {
          console.log('Something went wrong, and couldn\'t remove item from DB', e);
        });
    } else {
      // change for a snackbar
      console.log('You need to be logged in');
    }
  }
  static async saveForLater(data, eventTarget) {
    // Check if user is sign in
    if (firebase.auth().currentUser) {
      // If not saved, saved, if not remove from DB and DOM
      if (!await Controller.checkIfSaved(data.id)) {
        console.log('Saving movie');
        console.log(data);
        // Data from HTML Element attributes doesnt come with cast,
        // we need to get it from data saved in memory
        let movieDetails = Model.getMovie(data.id);
        data.cast = movieDetails.cast;
        // Save reference to DB path
        let reference = firebase.database().ref('users/' + firebase.auth().currentUser.uid + '/movies/').push();
        // Save Firebase key to local object
        data.key = reference.key;
        // Push data to path in current reference
        reference.set({
          id: data.id,
          title: data.title,
          summary: data.summary,
          small_cover_image: data.small_cover_image,
          medium_cover_image: data.medium_cover_image,
          year: data.year,
          rating: data.rating,
          yt_trailer_code: data.yt_trailer_code,
          cast: data.cast || null,
          notif: false,
        }).then(() => {
          console.log('Movie saved');
          eventTarget.innerHTML = 'remove_circle';
          data.isSaved = true;
          let containerEl = document.getElementById('list-container-saved');
          // Aadd item to Saved Tab container
          View.preppendItem(containerEl, TEMPLATES.movieListItem(data));
        });
      } else {
        console.log('Movie already saved');
        Controller.removeFromDB(data.key, eventTarget, data);
      }
    } else {
      View.showSnackBar('You need to sign in', this.signin, 'Login');
    }
  }
  registerUser(userId, name, email) {
    console.log('registering user');
    this.database.ref('users/' + userId).set({
        name: name,
        email: email,
        movies: {},
      })
      .then(() => {
        console.log('User successfully registered');
      })
      .catch((error) => {
        console.log('Error registering user', error);
      });
  }
  // Check when user is logged in
  onAuthStateChanged(user) {
    // Chec if user is sign in
    if (user) {
      // Controller.currentUser = user;
      // Reference login button
      let loginBanner = document.getElementById('loginBanner');
      // Hide login banner
      loginBanner.setAttribute('hidden', 'true');
      // Add name, progile picture and logout button on top of container
      this.showProfileInfo(user);
      // Refernce to logout button once appended to DOM
      let logoutButton = document.getElementById('logoutButton');
      // Add event listener to signout button
      logoutButton.addEventListener('click', () => {
        this.signOut();
      });
      this.getSavedMovies();
    } else {
      // If not logged in, hide profile element, then show login banner
      let profileInfo = document.getElementById('profileInfo');
      // Remove only if was previously inserted into DOM (after logout)
      if (profileInfo) {
        profileInfo.remove();
      }
      let loginBanner = document.getElementById('loginBanner');
      loginBanner.removeAttribute('hidden');
    }
  }
  // Append profileInfo template to DOM
  showProfileInfo(user) {
    let containerNode = document.getElementById('saved-panel');
    View.preppendItem(containerNode, TEMPLATES.profileInfo(user));
  }
  /**
   *  Create a new View with the current container element
   */
  createView() {
    this.view = new View(this.containerId);
  }
  /** Bring to view popular and latest uploaded movies
   */
  getMovies() {
    // we need this array to compute popular movies
    let moviesList = [];
    let moviesListMapped = [];
    // Select only the recent movies sections, not the enelire list
    // container
    let containerEl = document.getElementById('recentMoviesSection');
    // We need to return this as a primse so we can wait for this to succed,
    // before we can compute popular movies
    let getMovies = new Promise((resolve, reject) => {
      // Once we get the json data from the API, we desiplay every
      // movie item, and get its details
      Model.getLatestMovies(50)
        .then((json) => {
          this.view.displayElements(
            containerEl,
            json.data.movies,
            TEMPLATES.movieListItem);
          for (let movie of json.data.movies) {
            // Push a promise to an array if promises that will resolve
            // with a list of movie details
            moviesList.push(Model.fetchMovieDetails(movie.id));
          }
          resolve(json.data.movies);
        })
        .catch((error) => {
          console.log(error.message);
        });
    });
    (async() => {
      // Get list of latest 50 movies
      await getMovies;
      // We await to resolve every getMovieDetails promise, and map the
      // responses to a new array
      await Promise.all(moviesList).then((responses) => {
        return responses.map((response) => {
          return moviesListMapped.push(response.data.movie);
        });
      });
      // Once we get the extra data for every movie into a new array,
      // we can compute the 4 most popular
      Model.saveData(moviesListMapped);
      this._computePopularMovies(moviesListMapped);
    })();
  }
  /** Cumpute popular movies based on download count
   * @param  {map} movies - A map with the latest movies and details from
   *                        each one
   */
  _computePopularMovies(movies) {
    // sort movies by download count property
    movies.sort((a, b) => {
      return b['download_count'] - a['download_count'];
    });
    movies = movies.slice(0, 4);
    // movies = movies.filter((value) => {
    //     return value.date_uploaded_unix >= (Math.floor(Date.now()/1000))-
    //         2592000;
    // }).slice(0, 6);
    // Sort by date the result from the last sorting
    movies.sort((a, b) => {
      return b['date_uploaded_unix'] - a['date_uploaded_unix'];
    });
    // Now display the most popular movies
    let containerEl = document.getElementById('popularMoviesSection');
    // Pass ordered movies and the right template
    this.view.displayElements(
      containerEl,
      movies,
      TEMPLATES.popularMovieItem);
    // Fix strange sticky behavior on list headers
    let stickyElements = document.getElementsByClassName('list-header');
    [].forEach.call(stickyElements, (element) => {
      element.classList.add('sticky');
    });
  }
}
